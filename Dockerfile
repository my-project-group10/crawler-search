FROM alpine
#ENV MONGO mongodb
#ENV MONGO_PORT mongodb-port
#ENV RMQ_HOST rabbitmq
#ENV RMQ_QUEUE rabbitmq-queue
#ENV RMQ_USERNAME rabbitmq-user
#ENV RMQ_PASSWORD rabbitmq-password
#ENV CHECK_INTERVAL 10
#ENV EXCLUDE_URLS .*github.com
WORKDIR crawler
COPY ./ /crawler
RUN apk add --no-cache python3 py3-pip \
    && apk add --no-cache --virtual build-dependencies git \
    && pip3 install -r /crawler/requirements.txt \
    && pip3 install requests \
    && apk del build-dependencies
ENTRYPOINT sleep 20 && python3 -u crawler/crawler.py https://vitkhab.github.io/search_engine_test_site/
